
public interface Component
{
	boolean contains(Component c);
}
