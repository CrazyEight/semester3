import java.util.Vector;

public abstract class Container implements Component
{
	public Vector<Containee> parts;

	/*
	 * 4a)
	 */
	@Override
	public boolean contains(Component c)
	{
		if (this.equals(c))
			return true;
		for (Containee part : parts)
		{
			if (part.contains(c))
				return true;
		}
		return false;
	}

	/*
	 * 4b)
	 */
	public void add(Containee c) throws CycleException
	{
		// 4e
		synchronized (Global.g)
		{
			if (c.contains(this))
				throw new CycleException();
			this.parts.add(c);
		}
	}
}
