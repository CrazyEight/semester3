
package observer;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

abstract public class Observee<E> {
	 		
	private Collection<Observer<E>> currentObservers;
	protected Observee(){
		this.currentObservers = new LinkedList<Observer<E>>();
	}
	/**Attaches an observer to this observee. 
	 * The same observer can only be registered once.
	 * @param observer receives update notifications 
	 * until deregister is called for this observer.
	 */
	public void register(Observer<E> observer){
		if (!this.currentObservers.contains(observer)) 
			this.currentObservers.add(observer);
	}
	/**Detaches an observer from this observee. 
	 * @param observer does no longer get update notifications
	 * until register is called for this observer.
	 */
	public void deregister(Observer<E> observer){
		this.currentObservers.remove(observer);
	}
	/**Sends update notifications to all registered observers
	 */
	protected void notifyObservers(E e){
		Iterator<Observer<E>> i = this.currentObservers.iterator();
		while (i.hasNext()) {
			i.next().update(e);
		}
	}
}
