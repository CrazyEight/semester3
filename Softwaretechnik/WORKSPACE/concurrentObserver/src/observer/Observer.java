package observer;


public interface Observer<E> {
	
	/**Operation that each observer needs to implement in 
	 * order to receive notifications from observee.
	 */
	public void update(E e);
}
