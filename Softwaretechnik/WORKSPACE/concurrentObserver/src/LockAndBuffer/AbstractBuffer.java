package LockAndBuffer;

import java.util.LinkedList;
import java.util.List;

abstract public class AbstractBuffer<E> {

	protected List<Entry<E>> data;
	private int capacity;

	AbstractBuffer(int capacity){
		if (capacity < 1) throw new Error();
		this.capacity = capacity;
		this.data = new LinkedList<Entry<E>>();
	}
	protected boolean isEmpty() {
		return this.data.isEmpty(); 
	}
	protected boolean isFull() {
		return this.data.size() == capacity;
	}
	public void put(E e) {
		this.put(new Wrapper<E>(e));
	}
	public void stop() {
		this.put(new Stop<E>());
	}
	E getEntry() throws StopException {
		E result = this.data.get(0).getEntry();
		this.data.remove(0);
		return result;
	}
	abstract public E get() throws StopException;
	abstract void put(Entry<E> wrapper);
}
