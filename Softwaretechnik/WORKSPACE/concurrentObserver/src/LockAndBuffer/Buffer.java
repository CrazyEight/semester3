package LockAndBuffer;


/**
 *  Buffer that is used by one reader and one writer.
 *  Writer can stop the the reader by putting Stop-Command into buffer! 
 */
public class Buffer<E> extends AbstractBuffer<E> {
		
	final Lock writersSleep;
	final Lock readersSleep;
	int writersWaiting;
	int readersWaiting;
	final Lock mutex;
	
	public Buffer(int capacity) {
		super(capacity);
		this.writersSleep = new Lock(true);  //initially closed
		this.readersSleep = new Lock(true);  //initially closed
		this.mutex = new Lock(false); //initially open
		this.readersWaiting = 0;
		this.writersWaiting = 0;
	}
	public E get() throws StopException{
		this.mutex.lock();				//synchronized (this){...
		while (this.isEmpty()) {
			this.readersWaiting = this.readersWaiting + 1;      // \
			this.mutex.unlock();								//  \
			this.readersSleep.lock();							//   this.wait
			this.mutex.lock();									//  /
		}
		E result = getEntry();
		if (this.writersWaiting > 0) {							// \
			this.writersWaiting = this.writersWaiting - 1;		//	this.notify
			this.writersSleep.unlock();							// /
		}
		this.mutex.unlock();									//synchronized (this) ...}
		return result;
	}
	protected void put(Entry<E> e) {
		this.mutex.lock();										//synchronized (this){...
		while (this.isFull()) {
			this.writersWaiting = writersWaiting + 1;        	// \
			this.mutex.unlock();								//  \
			this.writersSleep.lock();							//   this.wait
			this.mutex.lock();									//  /			
		}
		this.data.add(e);
		if (this.readersWaiting > 0) {							// \
			this.readersWaiting = readersWaiting - 1;			//	this.notify
			this.readersSleep.unlock();							// /
		}
		this.mutex.unlock();									//synchronized (this) ...}
	}

}
