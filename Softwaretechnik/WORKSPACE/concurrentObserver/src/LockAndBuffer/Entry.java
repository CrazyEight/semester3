package LockAndBuffer;

abstract class Entry<E>
{
	abstract public E getEntry() throws StopException;
}

class Stop<E> extends Entry<E>
{
	@Override
	public E getEntry() throws StopException
	{
		throw new StopException();
	}
}

class Wrapper<E> extends Entry<E>
{
	private final E data;

	public Wrapper(E e)
	{
		this.data = e;
	}

	@Override
	public E getEntry()
	{
		return this.data;
	}
}
