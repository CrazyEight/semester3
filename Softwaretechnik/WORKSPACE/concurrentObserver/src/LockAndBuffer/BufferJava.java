package LockAndBuffer;

public class BufferJava<E> extends AbstractBuffer<E>{

	public BufferJava(int capacity) {
		super(capacity);
	}

	@Override
	synchronized public E get() throws StopException {
		while (this.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
		this.notify();
		return this.getEntry();
	}

	@Override
	synchronized void put(Entry<E> e) {
		while (this.isFull()) {
			try {
				this.wait();
			} catch (InterruptedException ie) {
				throw new Error(ie);
			}
		}
		this.data.add(e);
		this.notify();
	}

}
