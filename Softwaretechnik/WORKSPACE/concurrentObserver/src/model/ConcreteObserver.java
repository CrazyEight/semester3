package model;
import java.util.Random;

import application.Application;
import observer.Observer;

abstract public class ConcreteObserver implements Observer<Integer> {
	
	private static final int CalculationTime = 5000;
	private static final Random random = new Random();
	
	ConcreteObserverViewer view;
	ConcreteObservee observee;
	
	protected ConcreteObserver(ConcreteObserverViewer view){
		this.view = view;
		this.observee = Application.getObservee();
	}

	Object delay = new Object();
	/**Simulates a calculation that can last at most for the time 
	 * configured in the constant CalculationTime in milli-seconds.
	 */
	private void doSomeCalculations(){
		synchronized(delay) {
			try {
				this.delay.wait((long) (CalculationTime * random.nextFloat()) + 1);
			} catch (InterruptedException e) {}
		}
	} 

	abstract public void update(Integer i);

	boolean toggle = true;
	protected void doTheUpdate(Integer i) {
		int newValue = toggle ? this.view.getValue() + i : this.view.getValue() - i;
		toggle = ! toggle;
		this.doSomeCalculations();
		this.view.setValue(newValue);
	}
	
}
