package model;

import LockAndBuffer.AbstractBuffer;
import LockAndBuffer.Buffer;
import LockAndBuffer.BufferJava;
import LockAndBuffer.StopException;
import view.ConcreteObserverView;

public class ConcreteObserverParallel extends ConcreteObserver implements Runnable{

	public static ConcreteObserver create(ConcreteObserverView view) {
		return new ConcreteObserverParallel(view);
	}
	private Thread updater;

	private AbstractBuffer<Integer> buffer;
	
	protected ConcreteObserverParallel(ConcreteObserverViewer view) {
		super(view);
		this.buffer = new Buffer<Integer>(5);
		this.updater = new Thread(this);
		this.updater.start();
	}
	public void run() {
		while(true) {			
			int newValue;
			try {
				newValue = this.buffer.get();
			} catch (StopException e) {
				throw new Error(e);
			}
			this.doTheUpdate(newValue);
		}
	}

	@Override
	public void update(Integer i) {
		this.buffer.put(i);
	}

}






