package Buffer;

public class Wrapper<E> extends Entry<E>
{
	private E entry;
	
	public Wrapper(E e)
	{
		this.entry = e;
	}
	
	@Override
	public E getEntry()
	{
		return this.entry;
	}

	@Override
	public String toString()
	{
		return this.entry.toString();
	}

}
