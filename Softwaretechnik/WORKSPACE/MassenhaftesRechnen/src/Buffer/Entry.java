package Buffer;

public abstract class Entry<E>
{
	abstract E getEntry() throws StopException;
	
	public abstract String toString();
}
