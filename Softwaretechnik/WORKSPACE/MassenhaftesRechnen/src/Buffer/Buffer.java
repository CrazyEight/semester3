package Buffer;

import java.util.LinkedList;
import java.util.List;

import ArithmeticalExpression.ErrorValueException;

public class Buffer<E>
{
	List<Entry<E>> data;
	
	public Buffer()
	{
		this.data = new LinkedList<Entry<E>>();
	}

	public void put(E e)
	{
		this.put(new Wrapper(e));
	}
	
	public void stop()
	{
		this.put(new StopEntry<E>());
	}
	
	@Override
	synchronized public String toString()
	{
		String outputString = "";
		for (Entry<E> entry : data)
		{
			outputString += entry.toString() + " ";
		}
		return outputString;
	}

	private E getEntry() throws StopException
	{
		E result = this.data.get(0).getEntry();
		this.data.remove(0);
		return result;
	}

	synchronized public E get() throws StopException
	{
		while (this.data.isEmpty())
		{
			try
			{
				this.wait();
			} catch (InterruptedException e)
			{
				throw new Error(e);
			}
		}
		return this.getEntry();
	}

	synchronized private void put(Entry<E> e)
	{
		this.data.add(e);
		this.notify();
	}

	public void putErrorValue()
	{
		this.put(new Wrapper(new ErrorValueException()));
	}
}
