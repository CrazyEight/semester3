package ArithmeticalExpression;

import Buffer.Buffer;
import Buffer.StopException;

public class Sub extends Operation
{
	public static Sub create(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer) throws StopException
	{
		return new Sub(firstInputBuffer, secondInputBuffer);
	}

	private Sub(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer)
	{
		super (firstInputBuffer, secondInputBuffer);
	}

	@Override
	Integer calculate(int x1, int x2)
	{
		return x1 - x2;
	}
}