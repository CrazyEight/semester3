package ArithmeticalExpression;

import Buffer.Buffer;
import Buffer.StopException;

public class Div extends Operation
{
	public static Div create(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer) throws StopException
	{
		return new Div(firstInputBuffer, secondInputBuffer);
	}

	private Div(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer)
	{
		super (firstInputBuffer, secondInputBuffer);
	}

	@Override
	Integer calculate(int x1, int x2) throws ErrorValueException
	{
		if (x2 == 0) {
			throw new ErrorValueException();
		}
		return x1 / x2;
	}
}