package ArithmeticalExpression;

import Buffer.Buffer;
import Buffer.StopException;

public class Mul extends Operation
{
	public static Mul create(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer) throws StopException
	{
		return new Mul(firstInputBuffer, secondInputBuffer);
	}

	private Mul(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer)
	{
		super (firstInputBuffer, secondInputBuffer);
	}

	@Override
	Integer calculate(int x1, int x2)
	{
		return x1 * x2;
	}
}