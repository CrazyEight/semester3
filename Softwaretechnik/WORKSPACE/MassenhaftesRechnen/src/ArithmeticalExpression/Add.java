package ArithmeticalExpression;

import Buffer.Buffer;
import Buffer.StopException;

public class Add extends Operation
{
	public static Add create(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer) throws StopException
	{
		return new Add(firstInputBuffer, secondInputBuffer);
	}

	private Add(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer)
	{
		super (firstInputBuffer, secondInputBuffer);
	}

	@Override
	Integer calculate(int x1, int x2)
	{
		return x1 + x2;
	}
}
