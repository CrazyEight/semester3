package ArithmeticalExpression;

import Buffer.Buffer;
import Buffer.StopException;

public abstract class Operation extends Expression implements Runnable
{
	protected Thread operationThread;
	protected Buffer<Integer> firstInputBuffer;
	protected Buffer<Integer> secondInputBuffer;
	protected Buffer<Integer> outputBuffer;
	
	public Operation(Buffer<Integer> firstInputBuffer, Buffer<Integer> secondInputBuffer)
	{
		this.firstInputBuffer = firstInputBuffer;
		this.secondInputBuffer = secondInputBuffer;
		this.outputBuffer = new Buffer<Integer>();
		this.operationThread = new Thread(this);
		this.operationThread.start();
	}

	abstract Integer calculate(int x1, int x2) throws ErrorValueException;
	
	public Buffer<Integer> getOutputBuffer()
	{
		return this.outputBuffer;
	}
	
	@Override
	public void run()
	{
		while (true)
		{
			try
			{
				int x1 = this.firstInputBuffer.get();
				int x2 = this.secondInputBuffer.get();
				try
				{
					this.outputBuffer.put(this.calculate(x1, x2));
				} catch (ErrorValueException e)
				{
					this.outputBuffer.putErrorValue();
				}
			} catch (StopException e)
			{
				this.outputBuffer.stop();
				break;
			}
		}
	}
}
