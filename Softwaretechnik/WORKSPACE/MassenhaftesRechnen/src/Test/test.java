package Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ArithmeticalExpression.Add;
import ArithmeticalExpression.Div;
import ArithmeticalExpression.ErrorValueException;
import ArithmeticalExpression.Mul;
import ArithmeticalExpression.Sub;
import Buffer.Buffer;
import Buffer.StopException;

class test
{
	@Test
	void addTest() throws StopException
	{
		Buffer<Integer> buffer1 = new Buffer<>();
		Buffer<Integer> buffer2 = new Buffer<>();
		
		buffer1.put(5);
		buffer1.put(10);
		buffer1.put(15);
		buffer1.stop();
		
		buffer2.put(2);
		buffer2.put(3);
		buffer2.put(4);
		buffer2.stop();
		
		Add add = Add.create(buffer1, buffer2);
		
		Buffer<Integer> result = add.getOutputBuffer();
		assertEquals(7, result.get().intValue());
		assertEquals(13, result.get().intValue());
		assertEquals(19, result.get().intValue());
		
		try {
			result.get();
		} catch (Exception e) {
			assertEquals(true, e instanceof StopException);
		}
	}
	
	@Test
	void subTest() throws StopException
	{
		Buffer<Integer> buffer1 = new Buffer<>();
		Buffer<Integer> buffer2 = new Buffer<>();
		
		buffer1.put(8);
		buffer1.put(4);
		buffer1.put(3);
		buffer1.stop();
		
		buffer2.put(7);
		buffer2.put(9);
		buffer2.put(23);
		buffer2.stop();
		
		
		Sub sub = Sub.create(buffer1, buffer2);
		
		Buffer<Integer> result = sub.getOutputBuffer();
		assertEquals(1, result.get().intValue());
		assertEquals(-5, result.get().intValue());
		assertEquals(-20, result.get().intValue());
		
		try {
			result.get();
		} catch (Exception e) {
			assertEquals(true, e instanceof StopException);
		}
	}
	
	@Test
	void mulTest() throws StopException
	{
		Buffer<Integer> buffer1 = new Buffer<>();
		Buffer<Integer> buffer2 = new Buffer<>();
		
		buffer1.put(2);
		buffer1.put(6);
		buffer1.put(8);
		buffer1.stop();
		
		buffer2.put(0);
		buffer2.put(3);
		buffer2.put(4);
		buffer2.stop();
		
		
		Mul mul = Mul.create(buffer1, buffer2);
		
		Buffer<Integer> result = mul.getOutputBuffer();
		
		assertEquals(0, result.get().intValue());
		assertEquals(18, result.get().intValue());
		assertEquals(32, result.get().intValue());
		
		try {
			result.get();
		} catch (Exception e) {
			assertEquals(true, e instanceof StopException);
		}
	}
	
	@Test
	void divTest() throws StopException
	{
		Buffer<Integer> buffer1 = new Buffer<>();
		Buffer<Integer> buffer2 = new Buffer<>();
		
		buffer1.put(10);
		buffer1.put(6);
		buffer1.put(8);
		buffer1.stop();
		
		buffer2.put(5);
		buffer2.put(0);
		buffer2.put(3);
		buffer2.stop();
		
		Div div = Div.create(buffer1, buffer2);
		
		Buffer<Integer> result = div.getOutputBuffer();
		
		assertEquals(2, result.get().intValue());
		
		try {
			result.get();
		} catch (Exception e) {
			assertEquals(true, e instanceof ErrorValueException);
		}
		
		assertEquals(2, result.get().intValue());
		
		try {
			result.get();
		} catch (Exception e) {
			assertEquals(true, e instanceof StopException);
		}
	}

}
