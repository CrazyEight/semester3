package model;

public class DebitEntry implements Entry
{
	Transfer transfer;
	
	public static DebitEntry create(Transfer transfer) {
		return new DebitEntry(transfer);
	}
	
	public DebitEntry(Transfer transfer) {
		this.transfer = transfer;
	}

	@Override
	public String toString() {
		return "TO " + this.transfer.getToAccount().getName() + " : (" + this.transfer.getAmount() + ") SUBJECT: " + this.transfer.getSubject();
	}

	@Override
	public <T> T acceptEntryVisitor(EntryVisitor<T> visitor)
	{
		return visitor.handleDebitEntry(this);
	}
}
