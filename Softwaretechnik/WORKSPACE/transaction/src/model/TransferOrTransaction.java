package model;

import States.BookingState;
import States.OpenState;

public abstract class TransferOrTransaction {
	
	public TransferOrTransaction()
	{
		this.state = OpenState.create(this);
	}
	
	protected BookingState state;
	/** Removes the receiver's <amount> from the receiver's <fromAccount> (debit)
	 *  and adds the receiver's <amount> to the receiver's <toAccount> (credit).
	 * @throws AccountException if the booking violates account limits. 
	 */
	public abstract void book() throws AccountException;
	
	public void changeState(BookingState state) {
		this.state = state;
	}
}
