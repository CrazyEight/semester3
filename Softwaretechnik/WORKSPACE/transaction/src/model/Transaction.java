package model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Transaction extends TransferOrTransaction {
	
	public static Transaction create() {
		return new Transaction();
	}

	private List<Transfer> transfers;
	private Stack<Transfer> doneTransfers;
	
	private Transaction() {
		super();
		this.transfers = new LinkedList<Transfer>();
		this.doneTransfers = new Stack<Transfer>();
	}
	public void addTransfer(Transfer transfer) {
		this.transfers.add(transfer);
	}
	public List<Transfer> getTransfers() {
		return this.transfers;
	}
	
	public void unbookDoneTransfers() {
		while(!this.doneTransfers.isEmpty()) {
			this.doneTransfers.pop().unbook();
		}
	}
	
	@Override
	public void book() throws AccountBalanceException {
		this.doneTransfers.clear();
		this.state.changeStateInProgress();
		// TODO DONE implement "book()" in Transaction!
		// Da eine Transaktion aus mehreren Transfers besteht wird �ber die 
		// transfers iteriert und f�r jeden Transfer book() aufgerufen
		Iterator<Transfer> allTransfers = this.transfers.iterator();
		while (allTransfers.hasNext()) {
			try {
				Transfer nextTransfer = allTransfers.next();
				nextTransfer.book();
				this.doneTransfers.push(nextTransfer);
			} catch (AccountBalanceException e){
				this.state.changeStateFailed();
				unbookDoneTransfers();
				throw e;
			}
		}
		this.state.changeStateFinished();
		System.out.println("Booking of transaction finished!");
	}
}
