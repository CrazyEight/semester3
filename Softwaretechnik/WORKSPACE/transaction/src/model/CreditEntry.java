package model;

public class CreditEntry implements Entry
{
	Transfer transfer;
	
	public static CreditEntry create(Transfer transfer) {
		return new CreditEntry(transfer);
	}
	
	public CreditEntry(Transfer transfer) {
		this.transfer = transfer;
	}

	@Override
	public String toString() {
		return "FROM " + this.transfer.getFromAccount().getName() + " : (" + this.transfer.getAmount() + ") SUBJECT: " + this.transfer.getSubject();
	}

	@Override
	public <T> T acceptEntryVisitor(EntryVisitor<T> visitor)
	{
		return visitor.handleCreditEntry(this);
	}
}
