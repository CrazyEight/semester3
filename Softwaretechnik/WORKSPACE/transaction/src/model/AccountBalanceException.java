package model;

@SuppressWarnings("serial")
public class AccountBalanceException extends AccountException
{
	private static final String AccountBalanceMessage = "The Balance of following account would fall under the set limit: ";
	
	public AccountBalanceException(String accountName)
	{
		super(AccountBalanceMessage + accountName);
	}

}
