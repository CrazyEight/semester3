package model;

public class Transfer extends TransferOrTransaction {

	public static Transfer create(Account from, Account to,	long amount, String purpose) throws TransferException {
		return new Transfer(from, to, amount, purpose);
	}
	final private Account fromAccount;
	final private Account toAccount;
	final private long amount;
	final private String subject;

	public Transfer( Account from, Account to, long amount, String purpose) throws TransferException {
		super();
		if( amount <= 0 ) throw new TransferException("Amount must be positive!");
		this.fromAccount = from;
		this.toAccount = to;
		
		this.amount = amount;
		this.subject = purpose;
	}
	public Account getFromAccount() {
		return fromAccount;
	}
	public Account getToAccount() {
		return toAccount;
	}
	public long getAmount() {
		return amount;
	}
	public String getSubject() {
		return this.subject;
	}
	
	public void unbook() {
		this.fromAccount.increaseBalance(amount);
		this.toAccount.reduceBalance(amount);
	}
	
	@Override
	public void book() throws AccountBalanceException {
		this.state.changeStateInProgress();
		// TODO Implement "book()" in Transfer! 
		/**
		 * Ein Transfer ist eine einmalige Buchung von-nach
		 * dementsprechend wird hier auf den jeweilig beteiligten Accounts der betrag
		 * entweder hinzugefügt oder abgezogen.
		 */
		if (this.fromAccount.getBalance() - this.amount >= this.getFromAccount().UniversalAccountLimit)
		{
			this.fromAccount.reduceBalance(this.amount);
			this.fromAccount.addDebitEntry(this);
			this.toAccount.increaseBalance(this.amount);
			this.toAccount.addCreditEntry(this);
			this.state.changeStateFinished();
			System.out.println("Booking of transfer: " + this.amount + " from " + this.fromAccount.getName() 
			+ " to " + this.toAccount.getName() + " finished!");
		}
		else {
			this.state.changeStateFailed();
			System.out.println("Booking of transfer failed!");
			throw new AccountBalanceException(this.fromAccount.getName());
		}
	}
}
