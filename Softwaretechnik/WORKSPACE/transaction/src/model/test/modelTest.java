package model.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Account;
import model.AccountBalanceException;
import model.Transaction;
import model.Transfer;
import model.TransferException;

class modelTest
{
	private static Account acc1;
	private static Account acc2;
	
	@BeforeEach 
	void init() {
		acc1 = Account.create("Account1");
		acc2 = Account.create("Account2");
	}

	@Test
	void TransferTest() throws TransferException, AccountBalanceException
	{
		Transfer transfer1 = Transfer.create(this.acc1, this.acc2, 500, "500");
		Transfer transfer2 = Transfer.create(this.acc2, this.acc1, 300, "300");
		
		assertEquals(0, this.acc1.getBalance());
		assertEquals(0, this.acc2.getBalance());
		transfer1.book();
		assertEquals(-500, this.acc1.getBalance());
		assertEquals(500, this.acc2.getBalance());
		transfer2.book();
		assertEquals(-200, this.acc1.getBalance());
		assertEquals(200, this.acc2.getBalance());
	}
	
	@Test
	void TransactionTest() throws TransferException, AccountBalanceException
	{
		Transfer transfer1 = Transfer.create(this.acc1, this.acc2, 500, "500");
		Transfer transfer2 = Transfer.create(this.acc2, this.acc1, 300, "300");
		Transaction transaction1 = Transaction.create();
		transaction1.addTransfer(transfer1);
		transaction1.addTransfer(transfer2);
		
		assertEquals(0, this.acc1.getBalance());
		assertEquals(0, this.acc2.getBalance());
		transaction1.book();

		assertEquals(-200, this.acc1.getBalance());
		assertEquals(200, this.acc2.getBalance());
	}
	
	@Test
	void TransferExceptionTest() throws TransferException, AccountBalanceException
	{
		Transfer transfer1 = Transfer.create(this.acc1, this.acc2, 1000, "1000");
		Transfer transfer2 = Transfer.create(this.acc1, this.acc2, 1, "1");
		assertEquals(0, this.acc1.getBalance());
		assertEquals(0, this.acc2.getBalance());
		transfer1.book();

		assertEquals(-1000, this.acc1.getBalance());
		assertEquals(1000, this.acc2.getBalance());
		// Wie kann man das hier ohne Lambda ausdruck erreichen?
		// gefunden auf: https://howtodoinjava.com/junit5/expected-exception-example/
		assertThrows(AccountBalanceException.class, () -> {
			transfer2.book();
		});
		assertEquals(-1000, this.acc1.getBalance());
		assertEquals(1000, this.acc2.getBalance());
	}
	
	@Test
	void UnbookTransactionTest() throws TransferException, AccountBalanceException
	{
		Transfer transfer1 = Transfer.create(this.acc1, this.acc2, 5, "5");
		Transfer transfer2 = Transfer.create(this.acc1, this.acc2, 10, "10");
		Transfer transfer3 = Transfer.create(this.acc1, this.acc2, 15, "15");
		Transfer transfer4 = Transfer.create(this.acc1, this.acc2, 20, "20");
		
		Transaction transaction1 = Transaction.create();
		
		transaction1.addTransfer(transfer1);
		transaction1.addTransfer(transfer2);
		transaction1.addTransfer(transfer3);
		transaction1.addTransfer(transfer4);
		
		assertEquals(0, this.acc1.getBalance());
		assertEquals(0, this.acc2.getBalance());
		transaction1.book();
		assertEquals(-50, this.acc1.getBalance());
		assertEquals(50, this.acc2.getBalance());
		
		Transfer transfer5 = Transfer.create(this.acc1, this.acc2, 5000, "5000");
		transaction1.addTransfer(transfer5);
		transaction1.book();
		
		assertEquals(-50, this.acc1.getBalance());
		assertEquals(50, this.acc2.getBalance());
	}
	
}
