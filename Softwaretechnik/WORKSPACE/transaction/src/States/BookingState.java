package States;

import model.TransferOrTransaction;

public abstract class BookingState
{
	protected TransferOrTransaction transferOrTransaction;
	
	protected BookingState(TransferOrTransaction tot)
	{
		this.transferOrTransaction = tot;
		System.out.println("State changed to: " + this.getClass().toString());
	}
	
	public void changeStateInProgress() {
		this.transferOrTransaction.changeState(InProgressState.create(this.transferOrTransaction));
	}
	
	public void changeStateFinished() {
		this.transferOrTransaction.changeState(FinishedState.create(this.transferOrTransaction));
	}
	
	public void changeStateOpen() {
		this.transferOrTransaction.changeState(OpenState.create(this.transferOrTransaction));
	}
	
	public void changeStateFailed() {
		this.transferOrTransaction.changeState(FailedState.create(this.transferOrTransaction));
	}
}
