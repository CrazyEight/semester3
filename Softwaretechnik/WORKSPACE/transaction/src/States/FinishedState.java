package States;

import model.TransferOrTransaction;

public class FinishedState extends BookingState
{
	private FinishedState(TransferOrTransaction tot) {
		super(tot);
	}
	
	public static FinishedState create(TransferOrTransaction tot) {
		return new FinishedState(tot);
	}
}
