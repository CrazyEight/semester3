package States;

import model.TransferOrTransaction;

public class FailedState extends BookingState
{
	private FailedState(TransferOrTransaction tot) {
		super(tot);
	}
	
	public static FailedState create(TransferOrTransaction tot) {
		return new FailedState(tot);
	}
}
