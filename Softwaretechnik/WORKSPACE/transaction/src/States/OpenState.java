package States;

import model.TransferOrTransaction;

public class OpenState extends BookingState
{
	private OpenState(TransferOrTransaction tot) {
		super(tot);
	}
	
	public static OpenState create(TransferOrTransaction tot) {
		return new OpenState(tot);
	}
}
