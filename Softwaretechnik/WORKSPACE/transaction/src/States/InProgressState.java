package States;

import model.TransferOrTransaction;

public class InProgressState extends BookingState
{
	private InProgressState(TransferOrTransaction tot) {
		super(tot);
	}
	
	public static InProgressState create(TransferOrTransaction tot) {
		return new InProgressState(tot);
	}
}
