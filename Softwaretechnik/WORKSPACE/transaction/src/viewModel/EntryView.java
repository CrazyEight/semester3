package viewModel;

import model.CreditEntry;
import model.DebitEntry;
import model.Entry;
import model.EntryVisitor;

public class EntryView {

	protected static final String DebitPrefix = "Debit: ";
	protected static final String CreditPrefix = "Credit: ";

	public static EntryView create(Entry entry) {
		return new EntryView(entry);
	}
	private Entry entry;
	
	public EntryView(Entry entry) {
		this.entry = entry;
	}
	
	public String toString(){
		//TODO DONE Implement reasonable string representation!!!
		return this.entry.acceptEntryVisitor(new EntryVisitor<String>(){

			@Override
			public String handleDebitEntry(DebitEntry debitEntry)
			{
				return debitEntry.toString();
			}

			@Override
			public String handleCreditEntry(CreditEntry creditEntry)
			{
				return creditEntry.toString();
			}
		});

	}

}
