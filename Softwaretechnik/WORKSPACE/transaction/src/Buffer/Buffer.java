package Buffer;

import java.util.LinkedList;
import java.util.List;

public class Buffer<E>
{
	List<Entry<E>> data;
	
	public Buffer()
	{
		this.data = new LinkedList<Entry<E>>();
	}
	
	public void put (E e) {
		this.put(new Wrapper<E>(e));
	}
	
	public void stop() {
		this.put(new Stop<E>());
	}
	
	private boolean isEmpty() {
		return this.data.isEmpty();
	}
	
	private E getEntry() throws StopException {
		E result = this.data.get(0).getEntry();
		this.data.remove(0);
		return result;
	}
	
	synchronized public E get() throws StopException {
		while (this.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
		return this.getEntry();
	}

	private synchronized void put(Entry<E> e) {
		this.data.add(e);
		this.notify();
	}
}
