package Buffer;

public abstract class Entry<E>
{
	abstract E getEntry() throws StopException;
}

class Wrapper<E> extends Entry<E>{
 
	private E entry;
	
	public Wrapper(E entry)
	{
		this.entry = entry;
	}
	
	@Override
	E getEntry()
	{
		return this.entry;
	}
	
}

class Stop<E> extends Entry<E>{

	@Override
	E getEntry() throws StopException
	{
		throw new StopException();
	}
	
}
