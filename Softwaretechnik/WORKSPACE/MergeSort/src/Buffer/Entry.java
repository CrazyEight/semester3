package Buffer;

public abstract class Entry<E> {

	public abstract E getEntry() throws StopException;
}
