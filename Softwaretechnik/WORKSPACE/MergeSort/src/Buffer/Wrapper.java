package Buffer;

public class Wrapper<E> extends Entry<E>{

	private E wrappedElement;
	
	public Wrapper(E element) {
		this.wrappedElement = element;
	}

	@Override
	public E getEntry() {
		return this.wrappedElement;
	}
	
	@Override
	public String toString()
	{
		return this.wrappedElement.toString();
	}
}
