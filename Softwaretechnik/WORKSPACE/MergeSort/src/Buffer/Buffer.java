package Buffer;

import java.util.LinkedList;
import java.util.List;

public class Buffer<E> {

	private List<Entry<E>> data;

	public Buffer() {
		this.data = new LinkedList<>();
	}
	
	@Override
	public String toString()
	{
		String resultString = "";
		for (Entry<E> entry : data) {
			resultString += entry.toString() + " ";
		}
		return resultString;
	}
	
	public int getSize() {
		return this.data.size();
	}

	private synchronized void put(Entry<E> entry) {
		this.data.add(entry);
		this.notify();
	}

	public void put(E entry) {
		this.put(new Wrapper<E>(entry));
	}

	public synchronized E get() throws StopException {
		while (this.data.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
		E result;

		result = this.data.get(0).getEntry();

		this.data.remove(0);

		return result;
	}

	public void stop() {
		this.put(new StopEntry<E>());
	}

	public Buffer<E> getLeftHalf() {
		int halfIndex = (this.data.size() - 1) / 2;
		Buffer<E> outputBuffer = new Buffer<E>();
		for(int i = 0; i < halfIndex; i++) {
			outputBuffer.put(this.data.get(i));
		}
		outputBuffer.stop();
		return outputBuffer;
	}

	public Buffer<E> getRightHalf() {
		int halfIndex = (this.data.size() - 1) / 2;
		Buffer<E> outputBuffer = new Buffer<E>();
		for(int i = halfIndex; i < this.data.size(); i++) {
			outputBuffer.put(this.data.get(i));
		}
		return outputBuffer;
	}

	public void add(Buffer<E> buffer) {
		for (int i = 1; i <= buffer.getSize(); i++) {
			try {
				this.put(buffer.get());
			} catch (StopException e) {
				this.stop();
			}
		}
	}
}
