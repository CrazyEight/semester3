package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

import Buffer.Buffer;
import Buffer.StopEntry;
import Buffer.StopException;
import Buffer.Wrapper;
import model.MergeSort;

class mergeSortTest {

	@Test
	void test() throws StopException {
		Buffer<Integer> buffer = new Buffer<Integer>();
		buffer.put(5);
		buffer.put(3);
		buffer.put(8);
		buffer.put(2);
		buffer.put(7);
		buffer.stop();

		Buffer<Integer> bufferLeft = buffer.getLeftHalf();
		Buffer<Integer> bufferRight = buffer.getRightHalf();

		assertEquals(5, bufferLeft.get().intValue());
		assertEquals(3, bufferLeft.get().intValue());
		try {
			bufferLeft.get();
		} catch (Exception e) {
			assertTrue(e instanceof StopException);
		}

		assertEquals(8, bufferRight.get().intValue());
		assertEquals(2, bufferRight.get().intValue());
		assertEquals(7, bufferRight.get().intValue());
		try {
			bufferRight.get();
		} catch (Exception e) {
			assertTrue(e instanceof StopException);
		}
	}

	@Test
	void sortTest() throws StopException {
		Buffer<Integer> buffer = new Buffer<Integer>();
		buffer.put(5);
		buffer.put(3);
		buffer.put(8);
		buffer.put(2);
		buffer.put(2);
		buffer.stop();
		
		Buffer<Integer> resultBuffer = MergeSort.create(buffer).getOutputBuffer();
		
		assertEquals(2, resultBuffer.get().intValue());
		assertEquals(2, resultBuffer.get().intValue());
		assertEquals(3, resultBuffer.get().intValue());
		assertEquals(5, resultBuffer.get().intValue());
		assertEquals(8, resultBuffer.get().intValue());
		
	}
	
	@Test
	void sortTest2oderSo() throws StopException {
		Buffer<Integer> buffer = new Buffer<Integer>();
		buffer.put(5);
		buffer.put(3);
		buffer.stop();
	
		
		Buffer<Integer> resultBuffer = MergeSort.create(buffer).getOutputBuffer();
		
		assertEquals(3, resultBuffer.get().intValue());
		assertEquals(5, resultBuffer.get().intValue());
		
	}
	
	@Test
	void mergeTest() throws StopException {
		Buffer<Integer> buffer = new Buffer<Integer>();
		
		for (int i = 1; i <= 100; i++) {
			buffer.put(new Random().nextInt(10));
		}
		buffer.stop();
		System.out.println("unsorted: " + buffer.toString());
		
		Buffer<Integer> resultBuffer = MergeSort.create(buffer).getOutputBuffer();
		
		String resultString = "";
		for (int i = 1; i <= 50; i++) {
			resultString += resultBuffer.get().intValue() + " ";
		}
		System.out.println("sorted: " + resultString);
	}
	
	

}
