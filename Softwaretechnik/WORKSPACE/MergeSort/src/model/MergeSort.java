package model;

import Buffer.Buffer;
import Buffer.Entry;
import Buffer.StopException;

public class MergeSort implements Runnable
{

	private Buffer<Integer> inputBuffer;
	private Buffer<Integer> outputBuffer;
	// private Buffer<Integer> rightOutputBuffer;
	private Thread thread;

	private MergeSort(Buffer<Integer> unsortedBuffer)
	{
		this.inputBuffer = unsortedBuffer;
		this.outputBuffer = new Buffer<Integer>();
		// this.rightOutputBuffer = new Buffer<Integer>();
		this.thread = new Thread(this);
		this.thread.start();
	}

	public static MergeSort create(Buffer<Integer> unsortedBuffer)
	{
		return new MergeSort(unsortedBuffer);
	}

	@Override
	public void run()
	{
		if (this.inputBuffer.getSize() == 2)
		{
			try
			{
				Integer get = this.inputBuffer.get();
				this.outputBuffer.put(get);
				this.outputBuffer.stop();
			} catch (StopException e)
			{
				throw new Error(e);
			}
		} else
		{
			Buffer<Integer> left = this.inputBuffer.getLeftHalf();
			Buffer<Integer> right = this.inputBuffer.getRightHalf();
			MergeSort leftSort = MergeSort.create(left);
			MergeSort rightSort = MergeSort.create(right);

			this.merge(leftSort.getOutputBuffer(), rightSort.getOutputBuffer());
		}
	}

	private void merge(Buffer<Integer> leftBuffer, Buffer<Integer> rightBuffer)
	{
		Integer leftEntry = null;
		Integer rightEntry = null;

		while (true)
		{
			if (leftEntry == null)
			{
				try
				{
					leftEntry = leftBuffer.get();
				} catch (StopException e)
				{
					if (rightEntry != null)
					{
						this.outputBuffer.put(rightEntry);
					}
					this.outputBuffer.add(rightBuffer);
					break;
				}
			}
			if (rightEntry == null)
			{
				try
				{
					rightEntry = rightBuffer.get();
				} catch (StopException e)
				{
					if (leftEntry != null)
					{
						this.outputBuffer.put(leftEntry);
					}
					this.outputBuffer.add(leftBuffer);
					break;
				}
			}

			if (rightEntry != null && leftEntry != null)
			{
				if (leftEntry <= rightEntry)
				{
					this.outputBuffer.put(leftEntry);
					leftEntry = null;
				} else
				{
					this.outputBuffer.put(rightEntry);
					rightEntry = null;
				}

			} else
			{
				throw new Error("Fatal Error");
			}
		}
	}

	public Buffer<Integer> getOutputBuffer()
	{
		return this.outputBuffer;
	}
}
