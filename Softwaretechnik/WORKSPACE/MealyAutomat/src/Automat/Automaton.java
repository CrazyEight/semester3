package Automat;

import State.BeginningState;
import manager.ConfigurationManager;
import manager.NoConfigSucceededException;

public class Automaton {

	private final BeginningState beginningState;
	private ConfigurationManager manager;

	public static Automaton create(BeginningState beginningState) {
		return new Automaton(beginningState);
	}

	private Automaton(BeginningState beginningState) {
		this.manager = new ConfigurationManager();
		this.beginningState = beginningState;
	}

	public String run(String input) {
		Configuration.create(this.manager, this.beginningState, input, "");
		Configuration succeededConfiguration;
		try
		{
			succeededConfiguration = this.manager.getSucceededConfiguration();
		} catch (NoConfigSucceededException e)
		{
			return "No configuration succeeded with input: " + input;
		}
		return "Input: " + input + " Output: "  + succeededConfiguration.getOutput();
	}
}
