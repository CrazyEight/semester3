package Automat;

import State.State;
import manager.ConfigurationManager;

public class Configuration implements Runnable {

	private ConfigurationManager manager;
	private State currentState;
	private String remainingInput;
	private String output;

	private Thread thread;

	public static Configuration create(ConfigurationManager manager, State state, String remainingInput,
			String producedOutput) {
		return new Configuration(manager, state, remainingInput, producedOutput);
	}
	
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return this.currentState.toString();
	}

	private Configuration(ConfigurationManager manager, State state, String remainingInput, String producedOutput) {
		this.manager = manager;
		this.manager.register(this);
		this.currentState = state;
		this.remainingInput = remainingInput;
		this.output = producedOutput;
		this.thread = new Thread(this);
		this.thread.start();
	}

	@Override
	public void run() {
		if (this.remainingInput.isEmpty()) {
			if (this.currentState.isEndState()) {
				this.success();
			} else {
				this.fail();
			}
		} else {
			State[] possibleStates = this.currentState.readChar(this.remainingInput.charAt(0));
			if (possibleStates == null) {
				this.fail();
			} else {
				String remaining = this.remainingInput.substring(1);
				for (State state : possibleStates) {
					Configuration.create(this.manager, state, remaining, this.output + state.getOutputChar());
				}
				this.fail();
			}
		}
	}

	public String getOutput() {
		return output;
	}

	private void success() {
		this.manager.configurationSucceeded(this);
	}

	private void fail() {
		this.manager.configurationFailed(this);
	}

	public Thread getThread() {
		return this.thread;
	}

}
