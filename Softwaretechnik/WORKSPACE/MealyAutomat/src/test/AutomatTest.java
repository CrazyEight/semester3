package test;

import org.junit.Before;
import org.junit.Test;

import Automat.Automaton;
import State.BeginningState;
import State.EndState;
import State.InterimState;
import State.State;

public class AutomatTest {
	Automaton automaton;

	@Before
	public void createAutomaton() {
		BeginningState z0 = BeginningState.create("z0", 'a');
		InterimState z11 = InterimState.create("z11", 'b');
		InterimState z12 = InterimState.create("z12", 'd');
		InterimState z13 = InterimState.create("z13", 'f');
		InterimState z21 = InterimState.create("z21", 'c');
		InterimState z22 = InterimState.create("z22", 'e');
		EndState z23 = EndState.create("z23", 'g');

		this.automaton = Automaton.create(z0);

		z0.addSteps('0', new State[] { z11, z12, z13 });
		z11.addSteps('0', new State[] { z21 });
		z12.addSteps('0', new State[] { z22 });
		z12.addSteps('1', new State[] { z12 });
		z13.addSteps('1', new State[] { z23 });
		z23.addSteps('0', new State[] { z23 });
		z23.addSteps('1', new State[] { z22 });
	}

	@Test
	public void runConfig1() {
		this.createAutomaton();
		String result = this.automaton.run("010");
		System.out.println(result);
	}

	@Test
	public void runConfig2() {
		this.createAutomaton();
		String result = automaton.run("0101");
		System.out.println(result);
	}
	
	@Test
	public void runConfig3() {
		this.createAutomaton();
		String result = automaton.run("010000000000000000000000000000000000000000000");
		System.out.println(result);

	}
}
