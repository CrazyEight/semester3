package State;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import Visitor.StateVisitor;

public abstract class State {
	private String name;
	private char outputChar;
	private List<State> previousStates;
	private Map<Character, State[]> steps;

	protected State(String name, char outputChar) {
		this.name = name;
		this.outputChar = outputChar;
		this.steps = new HashMap<>();
		this.previousStates = new LinkedList<>();
	}
	
	public abstract boolean isBeginningState();
	public abstract boolean isEndState();

	@Override
	public String toString() {
		return this.name;
	}

	public List<State> getPreviousStates() {
		return this.previousStates;
	}

	public String getCurrentOutput() {
		String outputString = "";
		for (State state : this.previousStates) {
			outputString += state.getOutputChar() + " ";
		}
		return outputString;
	}

	public char getOutputChar() {
		return this.outputChar;
	}

	public void addSteps(char c, State[] states) {
		this.steps.put(c, states);
	}

	public State[] readChar(char c) {
		return steps.get(c);
	}

	public String previousStatesAsString() {
		String outputString = "";
		for (State state : this.previousStates) {
			outputString += state.toString() + " ";
		}
		return outputString;
	}

	public abstract void accept(StateVisitor visitor);
}
