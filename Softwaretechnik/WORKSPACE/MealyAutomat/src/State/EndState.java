package State;

import Visitor.StateVisitor;

public class EndState extends State
{
	protected EndState(String name, char outputChar) {
		super(name, outputChar);
	}
	
	public static EndState create(String name, char outputChar) {
		return new EndState(name, outputChar);
	}
	
	@Override
	public void accept(StateVisitor visitor)
	{
		visitor.handle(this);
	}

	@Override
	public boolean isBeginningState() {
		return false;
	}

	@Override
	public boolean isEndState() {
		return true;
	}
}
