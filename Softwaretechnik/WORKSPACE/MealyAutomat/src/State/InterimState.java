package State;

import Visitor.StateVisitor;

public class InterimState extends State
{
	protected InterimState(String name, char outputChar)
	{
		super(name, outputChar);
	}

	public static InterimState create(String name, char outputChar) {
		return new InterimState(name, outputChar);
	}
	
	@Override
	public void accept(StateVisitor visitor)
	{
		visitor.handle(this);
	}

	@Override
	public boolean isBeginningState() {
		return false;
	}

	@Override
	public boolean isEndState() {
		return false;
	}
}
