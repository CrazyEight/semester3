package State;

import Visitor.StateVisitor;

public class BeginningState extends State
{
	protected BeginningState(String name, char outputChar) {
		super(name, outputChar);
	}
	
	public static BeginningState create(String name, char outputChar) {
		return new BeginningState(name, outputChar);
	}

	@Override
	public void accept(StateVisitor visitor)
	{
		visitor.handle(this);
	}

	@Override
	public boolean isBeginningState() {
		return true;
	}

	@Override
	public boolean isEndState() {
		return false;
	}
}
