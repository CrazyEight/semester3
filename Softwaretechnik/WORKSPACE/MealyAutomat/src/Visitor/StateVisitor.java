package Visitor;

import State.BeginningState;
import State.EndState;
import State.InterimState;

public interface StateVisitor
{
	void handle(BeginningState state);
	void handle(EndState state);
	void handle(InterimState state);
}
