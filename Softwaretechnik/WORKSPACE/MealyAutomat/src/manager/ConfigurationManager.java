package manager;


import java.util.concurrent.ConcurrentLinkedQueue;

import Automat.Configuration;

public class ConfigurationManager {

	private ConcurrentLinkedQueue<Configuration> configurations;
	private Configuration succeededConfiguration;

	public ConfigurationManager() {
		this.configurations = new ConcurrentLinkedQueue<Configuration>();
	}

	public void register(Configuration configuration) {
		this.configurations.add(configuration);
	}

	public void configurationSucceeded(Configuration configuration) {
		for (Configuration config : this.configurations) {
			if (config != configuration)
				this.configurationFailed(config);
		}
		this.addSucceededConfiguration(configuration);
	}

	private synchronized void addSucceededConfiguration(Configuration configuration) {
		this.succeededConfiguration = configuration;
		this.notify();
	}

	public synchronized Configuration getSucceededConfiguration() throws NoConfigSucceededException {
		while (this.succeededConfiguration == null && !this.configurations.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
		if(this.succeededConfiguration != null) {
			return this.succeededConfiguration;			
		} else {
			throw new NoConfigSucceededException();
		}
	}
	
	@Override
	public String toString()
	{
		String outputString = "";
		for(Configuration c : this.configurations) {
			outputString += c.toString() + " ";
		}
		return outputString;
	}

	public synchronized void configurationFailed(Configuration configuration) {
		//System.out.println("Manager: " + this.toString() + " Failed: " + configuration.toString());
		configuration.getThread().interrupt();
		this.configurations.remove(configuration);
		if (this.configurations.isEmpty()) {
			this.notify();		
		}
	}
}
