package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import buffer.AbstractBuffer;
import buffer.BufferJava;
import buffer.StopException;
import model.OperationProcess;
import model.value.Value;
import operations.Add;
import operations.Divide;
import operations.Multiply;
import operations.Subtract;

class BufferTest {
	AbstractBuffer<Value> initial1_1;
	AbstractBuffer<Value> initial1_2;
	AbstractBuffer<Value> initial2_1;
	AbstractBuffer<Value> initial2_2;

	@BeforeEach
	void test() {

		initial1_1 = new BufferJava<Value>();
		initial1_2 = new BufferJava<Value>();
		initial2_1 = new BufferJava<Value>();
		initial2_2 = new BufferJava<Value>();
		
		Value int1 = Value.create(1);
		Value int2 = Value.create(2);
		Value int3 = Value.create(3);
		Value int4 = Value.create(4);
		Value int5 = Value.create(5);
		Value int6 = Value.create(6);
		Value int7 = Value.create(7);
		Value int8 = Value.create(8);
		
		initial1_1.put(int1);
		initial1_1.put(int2);
		initial1_1.put(int3);
		initial1_1.put(int4);
		initial1_1.put(int5);
		initial1_1.put(int6);
		initial1_1.put(int7);
		initial1_1.put(int8);
		initial1_1.stop();

		initial1_2.put(int1);
		initial1_2.put(int2);
		initial1_2.put(int3);
		initial1_2.put(int4);
		initial1_2.put(int5);
		initial1_2.put(int6);
		initial1_2.put(int7);
		initial1_2.put(int8);
		initial1_2.stop();
		

		initial2_1.put(int1);
		initial2_1.put(int2);
		initial2_1.put(int3);
		initial2_1.put(int4);
		initial2_1.put(int5);
		initial2_1.put(int6);
		initial2_1.put(int7);
		initial2_1.put(int8);
		initial2_1.stop();

		initial2_2.put(int1);
		initial2_2.put(int2);
		initial2_2.put(int3);
		initial2_2.put(int4);
		initial2_2.put(int5);
		initial2_2.put(int6);
		initial2_2.put(int7);
		initial2_2.put(int8);
		initial2_2.stop();

		System.out.println(initial1_1.toString());
		System.out.println(initial1_2.toString());
		

	}
	@Test
	void testAdd() {
		System.out.println("Add");
		OperationProcess addBuf1 = Add.create(initial1_1, initial1_2);
		OperationProcess addBuf2 = Add.create(initial2_1, initial2_2);
		OperationProcess addBuf3 = Add.create(addBuf1.getOutputBuffer(), addBuf2.getOutputBuffer());
		
	}
	@Test
	void testMul() {
		System.out.println("Mul");
		OperationProcess mulBuf1 = Multiply.create(initial1_1, initial1_2);
		OperationProcess mulBuf2 = Multiply.create(initial2_1, initial2_2);
		OperationProcess mulBuf3 = Multiply.create(mulBuf1.getOutputBuffer(), mulBuf2.getOutputBuffer());
		
	}
	@Test
	void testSub() {
		System.out.println("Sub");
		OperationProcess subBuf1 = Subtract.create(initial1_1, initial1_2);
		OperationProcess subBuf2 = Subtract.create(initial2_1, initial2_2);
		OperationProcess subBuf3 = Subtract.create(subBuf1.getOutputBuffer(), subBuf2.getOutputBuffer());
		
	}
	@Test
	void testDiv() {
		System.out.println("Div");
		OperationProcess divBuf1 = Divide.create(initial1_1, initial1_2);
		OperationProcess divBuf2 = Divide.create(initial2_1, initial2_2);
		OperationProcess divBuf3 = Divide.create(divBuf1.getOutputBuffer(), divBuf2.getOutputBuffer());
		
	}
	@Test
	void testAll() {
		AbstractBuffer<Value> initial3_1 = new BufferJava<Value>();

		Value int2 = Value.create(2);
		for(int i = 0; i < 8; i++) {
			initial3_1.put(int2);			
		}
		initial3_1.stop();
		System.out.println(initial3_1.toString());
		
		OperationProcess addBuf = Add.create(initial1_1, initial1_2);
		OperationProcess mulBuf = Multiply.create(addBuf.getOutputBuffer(), initial2_1);
		OperationProcess subBuf = Subtract.create(mulBuf.getOutputBuffer(), initial2_2);
		OperationProcess divBuf = Divide.create(subBuf.getOutputBuffer(), initial3_1);
		
	}
	@Test
	void testDivisionByZero() {
		AbstractBuffer<Value> initial1_0 = new BufferJava<Value>();
		
		Value int1 = Value.create(1);
		Value int0 = Value.create(0);
		
		initial1_0.put(int1);
		initial1_0.put(int1);
		initial1_0.put(int0);
		initial1_0.put(int1);
		initial1_0.put(int1);
		initial1_0.put(int0);
		initial1_0.put(int1);
		initial1_0.put(int0);
		initial1_0.stop();
		OperationProcess divBuf = Divide.create(initial1_1, initial1_0);
	}
	@Test
	void testDiffentLength() {
		try {
			initial1_1.get();
		} catch (StopException e) {}
		
		OperationProcess mulBuf1 = Multiply.create(initial1_1, initial1_2);
		OperationProcess mulBuf2 = Multiply.create(initial2_1, initial2_2);
		OperationProcess mulBuf3 = Multiply.create(mulBuf1.getOutputBuffer(), mulBuf2.getOutputBuffer());

	}
}
