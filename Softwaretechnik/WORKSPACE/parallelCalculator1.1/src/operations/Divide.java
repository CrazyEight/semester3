package operations;

import buffer.AbstractBuffer;
import buffer.StopException;
import model.OperationProcess;
import model.value.Value;

public class Divide extends OperationProcess implements Runnable {

	private Thread thread;

	private Divide(AbstractBuffer<Value> firstBuffer, AbstractBuffer<Value> secondBuffer) {
		super(firstBuffer, secondBuffer);
		this.thread = new Thread(this);
		this.thread.start();
		System.out.println("Thread is running");
	}

	public static Divide create(AbstractBuffer<Value> firstBuffer, AbstractBuffer<Value> secondBuffer) {
		return new Divide(firstBuffer, secondBuffer);
	}
	public void run() {
		while (true) {
			Value result;
			try {
				// TODO handle division by zero
				result = this.firstInputBuffer.get().divide(this.secondInputBuffer.get());
			} catch (StopException e) {
				this.outputBuffer.stop();
				System.out.println(this.outputBuffer.toString());
				this.thread.interrupt();
				System.out.println("Thread is interrupted");
				return;
			}
			this.outputBuffer.put(result);
		}
	}

}
