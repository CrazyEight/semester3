package operations;

import buffer.AbstractBuffer;
import buffer.StopException;
import model.OperationProcess;
import model.value.Value;

public class Multiply extends OperationProcess implements Runnable {

	private Thread thread;

	private Multiply(AbstractBuffer<Value> firstBuffer, AbstractBuffer<Value> secondBuffer) {
		super(firstBuffer, secondBuffer);
		this.thread = new Thread(this);
		this.thread.start();
		System.out.println("Thread is running");
	}

	public static Multiply create(AbstractBuffer<Value> firstBuffer, AbstractBuffer<Value> secondBuffer) {
		return new Multiply(firstBuffer, secondBuffer);
	}
	public void run() {
		while (true) {
			Value result;
			try {
				result = this.firstInputBuffer.get().multiply(this.secondInputBuffer.get());
			} catch (StopException e) {
				this.outputBuffer.stop();
				System.out.println(this.outputBuffer.toString());
				this.thread.interrupt();
				System.out.println("Thread is interrupted");
				return;
			}
			this.outputBuffer.put(result);
		}
	}

}
