package buffer;

import java.util.Iterator;

public class BufferJava<E> extends AbstractBuffer<E>{

	@Override
	synchronized public E get() throws StopException {
		while (this.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
		return this.getEntry();
	}

	@Override
	synchronized void put(Entry<E> e) {
		this.data.add(e);
		this.notify();
	}

	// TODO toString
	@Override
	public String toString() {
		StringBuffer string = new StringBuffer();
		Iterator<Entry<E>> i = this.data.iterator();
		while(i.hasNext()) {
			Entry<E> current = i.next();
			try {
				string.append(current.getEntry().toString() + ", ");
			} catch (StopException e) {
				string.append(" StopException");
			}
		}
		return string.toString();
	}

}
