package buffer;

import java.util.LinkedList;
import java.util.List;

abstract public class AbstractBuffer<E> {

	protected List<Entry<E>> data;

	protected AbstractBuffer(){
		this.data = new LinkedList<Entry<E>>();
	}
	protected boolean isEmpty() {
		return this.data.isEmpty(); 
	}
	public void put(E e) {
		this.put(new Wrapper<E>(e));
	}
	public void stop() {
		this.put(new Stop<E>());
	}
	protected E getEntry() throws StopException {
		E result = this.data.get(0).getEntry();
		this.data.remove(0);
		return result;
	}
	abstract public E get() throws StopException;
	abstract void put(Entry<E> wrapper);
	
}
