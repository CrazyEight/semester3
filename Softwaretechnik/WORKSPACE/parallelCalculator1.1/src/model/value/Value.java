package model.value;

public abstract class Value {

	public static Value create(int i) {
		return CorrectValue.create(i);
	}

	abstract Value accept(ValueVisitor valueVisitor);
	
	public abstract Value add(Value value);

	public abstract Value subtract(Value value);
	
	public abstract Value multiply(Value value);
	
	public abstract Value divide(Value value);
	
	@Override
	public abstract String toString();
	@Override
	public abstract boolean equals(Object argument);
}

