package model.value;

public class CorrectValue extends Value {

	public static CorrectValue create(int value) {
		return new CorrectValue(value);
	}

	private int value;

	private CorrectValue(Integer value) {
		this.value = value;
	}

	@Override
	public Value add(Value value) {
		Value result = value.accept(new ValueVisitor() {
			@Override
			public ErrorValue handleErrorValue(ErrorValue error) {
				return ErrorValue.getOrCreate();
			}

			@Override
			public CorrectValue handleCorrectValue(CorrectValue value) {
				int arg1 = CorrectValue.this.value;
				int arg2 = value.value;
				CorrectValue result = CorrectValue.create(arg1 + arg2);
				return result;
			}
		});
		return result;
	}

	@Override
	public Value subtract(Value value) {
		Value result = value.accept(new ValueVisitor() {
			@Override
			public ErrorValue handleErrorValue(ErrorValue error) {
				return ErrorValue.getOrCreate();
			}

			@Override
			public CorrectValue handleCorrectValue(CorrectValue value) {
				int arg1 = CorrectValue.this.value;
				int arg2 = value.value;
				CorrectValue result = CorrectValue.create(arg1 - arg2);
				return result;
			}
		});
		return result;
	}

	@Override
	public Value multiply(Value value) {
		Value result = value.accept(new ValueVisitor() {
			@Override
			public ErrorValue handleErrorValue(ErrorValue error) {
				return ErrorValue.getOrCreate();
			}

			@Override
			public CorrectValue handleCorrectValue(CorrectValue value) {
				int arg1 = CorrectValue.this.value;
				int arg2 = value.value;
				CorrectValue result = CorrectValue.create(arg1 * arg2);
				return result;
			}
		});
		return result;
	}

	@Override
	public Value divide(Value value) {
		Value result = value.accept(new ValueVisitor() {
			@Override
			public ErrorValue handleErrorValue(ErrorValue error) {
				return ErrorValue.getOrCreate();
			}

			@Override
			public Value handleCorrectValue(CorrectValue value) {
				int arg1 = CorrectValue.this.value;
				int arg2 = value.value;
				if(arg2 == 0) return ErrorValue.getOrCreate();
				CorrectValue result = CorrectValue.create(arg1 / arg2);
				return result;
			}
		});
		return result;
	}

	@Override
	Value accept(ValueVisitor valueVisitor) {
		return valueVisitor.handleCorrectValue(this);
	}

	@Override
	public String toString() {
		return this.value + "";
	}
	@Override
	public boolean equals(Object argument) {
		if(argument instanceof CorrectValue) {
			return this.value == ((CorrectValue) argument).value;
		}
		if(argument instanceof Integer) {
			return this.value == (Integer)argument;
		}
		return false;
		
	}
}
