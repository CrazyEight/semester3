package model.value;

public class ErrorValue extends Value{

	public static ErrorValue getOrCreate() {
		if(error == null) return new ErrorValue();
		return error;
	}
	private static ErrorValue error;
	
	private ErrorValue() {}
	@Override
	public Value add(Value value) {
		return ErrorValue.getOrCreate();
	}
	@Override
	public Value subtract(Value value) {
		return ErrorValue.getOrCreate();
	}
	@Override
	public Value multiply(Value value) {
		return ErrorValue.getOrCreate();
	}
	@Override
	public Value divide(Value value) {
		return ErrorValue.getOrCreate();
	}
	@Override
	Value accept(ValueVisitor valueVisitor) {
		return valueVisitor.handleErrorValue(this);
	}
	@Override
	public String toString() {
		return "Error";
	}
	@Override
	public boolean equals(Object argument) {
		if(argument instanceof ErrorValue) return true;
		return false;
	}

}
