package model.value; 

public interface ValueVisitor {

	ErrorValue handleErrorValue(ErrorValue error);
	
	Value handleCorrectValue(CorrectValue value);
}
