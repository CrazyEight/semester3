package model;

import buffer.AbstractBuffer;
import buffer.BufferJava;
import model.value.Value;

public abstract class OperationProcess {

	protected AbstractBuffer<Value> firstInputBuffer;
	protected AbstractBuffer<Value> secondInputBuffer;
	
	protected AbstractBuffer<Value> outputBuffer;
	
	protected OperationProcess(AbstractBuffer<Value> firstBuffer, AbstractBuffer<Value> secondBuffer) {
		this.firstInputBuffer = firstBuffer;
		this.secondInputBuffer = secondBuffer;
		this.outputBuffer = new BufferJava<Value>();
	}
	public AbstractBuffer<Value> getOutputBuffer(){
		return this.outputBuffer;
	}
	
}
