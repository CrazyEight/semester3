package model;

import Buffer.Buffer;
import Entry.StopException;

public class BubbleSortSequential
{	
	private Buffer<Comparable<Integer>> inputBuffer;
	private Buffer<Comparable<Integer>> outputBuffer;
	
	public static BubbleSortSequential create() {
		return new BubbleSortSequential();
	}
	
	private BubbleSortSequential()
	{
		this.inputBuffer = new Buffer<Comparable<Integer>>();
		this.outputBuffer = new Buffer<Comparable<Integer>>();
		
	}
	
	public Buffer<Comparable<Integer>> getOutputBuffer() {
		return this.outputBuffer;
	}
	
	public void sort(Buffer<Comparable<Integer>> unsortedBuffer)
	{	
		this.inputBuffer = unsortedBuffer;
		this.outputBuffer = new Buffer<Comparable<Integer>>();
		boolean changed = false;
		Comparable firstValue = null;
		Comparable secondValue = null;
		
		while(true)
		{
			try
			{
				if(firstValue == null) firstValue = inputBuffer.get();
				if(secondValue == null) secondValue = inputBuffer.get();
				if (firstValue.compareTo(secondValue) <= 0)
				{
					this.outputBuffer.put(firstValue);
					firstValue = secondValue;
					secondValue = null;
				}
				else if (firstValue.compareTo(secondValue) > 0)
				{
					this.outputBuffer.put(secondValue);
					changed = true;
					secondValue = null;
				}
			} catch (StopException e)
			{
				this.outputBuffer.put(firstValue);
				this.outputBuffer.stop();
				break;
			}
		}
		if(changed) {
			this.sort(this.getOutputBuffer());
		}
	}
}