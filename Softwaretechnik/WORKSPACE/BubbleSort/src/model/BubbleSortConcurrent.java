package model;

import Buffer.Buffer;
import Entry.StopException;

public class BubbleSortConcurrent implements Runnable
{
	Thread sortThread;
	Thread nextThread;
	private BubbleSortConcurrent nextThreadTarget;
	Buffer<Comparable<Integer>> inputBuffer;
	Buffer<Comparable<Integer>> outputBuffer;
	private boolean changed;
	private boolean nextTreadStarted = false;
	
	public static BubbleSortConcurrent create(Buffer<Comparable<Integer>> inputBuffer) {
		return new BubbleSortConcurrent(inputBuffer);
	}
	
	private BubbleSortConcurrent(Buffer<Comparable<Integer>> inputBuffer)
	{
		this.inputBuffer = inputBuffer;
		this.outputBuffer = new Buffer<Comparable<Integer>>();
		this.sortThread = new Thread(this);
		this.sortThread.start();
	}

	public Buffer<Comparable<Integer>> getOutputBuffer() {
		if(this.nextTreadStarted) {
			return this.nextThreadTarget.getOutputBuffer();
		}
		else {
			return this.outputBuffer;
		}
	}
	
	@Override
	public void run()
	{
		this.outputBuffer = new Buffer<Comparable<Integer>>();
		this.outputBuffer.clear();
		this.changed = false;
		Comparable firstValue = null;
		Comparable secondValue = null;
		
		while (!this.sortThread.isInterrupted()) {
			try
			{
				if (firstValue == null) firstValue = this.inputBuffer.get();
				if (secondValue == null) secondValue = this.inputBuffer.get();
				if (firstValue.compareTo(secondValue) <= 0)
				{
					this.outputBuffer.put(firstValue);
					firstValue = secondValue;
					secondValue = null;
				}
				else if (firstValue.compareTo(secondValue) > 0)
				{
					this.outputBuffer.put(secondValue);
					this.changed = true;
					secondValue = null;
					if (!this.nextTreadStarted) {
						this.nextThreadTarget = BubbleSortConcurrent.create(this.outputBuffer);
						this.nextThread = new Thread(this.nextThreadTarget);
						nextThread.start();
						this.nextTreadStarted = true;
					}
				}
			} catch (StopException e)
			{
				this.outputBuffer.put(firstValue);
				this.outputBuffer.stop();
				this.sortThread.interrupt();
			}
		}
	}
}
