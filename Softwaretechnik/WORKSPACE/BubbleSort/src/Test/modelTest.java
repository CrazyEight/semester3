package Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Buffer.Buffer;
import Entry.StopEntry;
import Entry.StopException;
import Entry.Value;
import model.BubbleSortConcurrent;
import model.BubbleSortSequential;

class modelTest {

	@Test
	void bubbleSortSequentialTest() throws StopException {
		Buffer<Comparable<Integer>> unsortedList = new Buffer<Comparable<Integer>>();
		Buffer<Comparable<Integer>> sortedList = new Buffer<Comparable<Integer>>();
		
		Comparable[] sortedArray = {1,2,3,4,5,6,7,8,9};
		sortedList.putArray(sortedArray);
		
		Comparable[] unsortedArray = {9,5,8,7,4,6,3,1,2};
		unsortedList.putArray(unsortedArray);
		
		System.out.println(unsortedList.toString());

		BubbleSortSequential bubbleSort = BubbleSortSequential.create();
		bubbleSort.sort(unsortedList);
		System.out.println(bubbleSort.getOutputBuffer().toString());
		
		Buffer<Comparable<Integer>> result = bubbleSort.getOutputBuffer();

		assertEquals(1, result.get());
		assertEquals(2, result.get());
		assertEquals(3, result.get());
		assertEquals(4, result.get());
		assertEquals(5, result.get());
		assertEquals(6, result.get());
		assertEquals(7, result.get());
		assertEquals(8, result.get());
		assertEquals(9, result.get());
	}
	
	
	@Test
	void bubbleSortConcurrentTest() throws StopException {
		Buffer<Comparable<Integer>> unsortedList = new Buffer<Comparable<Integer>>();
		Buffer<Comparable<Integer>> sortedList = new Buffer<Comparable<Integer>>();
		
		Comparable[] sortedArray = {1,2,3,4,5,6,7,8,9};
		sortedList.putArray(sortedArray);
		
		Comparable[] unsortedArray = {9,5,8,7,4,6,3,1,2};
		unsortedList.putArray(unsortedArray);
		
		System.out.println(unsortedList.toString());

		BubbleSortConcurrent bubbleSort = BubbleSortConcurrent.create(unsortedList);
		System.out.println("BubbleSortConcurrent output: "+ bubbleSort.getOutputBuffer().toString());
		
		Buffer<Comparable<Integer>> result = bubbleSort.getOutputBuffer();

		System.out.println(result.get() + ", " + result.get() + ", " + result.get() + ", " + result.get() + ", ");
//		assertEquals(1, result.get());
//		assertEquals(2, result.get());
//		assertEquals(3, result.get());
//		assertEquals(4, result.get());
//		assertEquals(5, result.get());
//		assertEquals(6, result.get());
//		assertEquals(7, result.get());
//		assertEquals(8, result.get());
//		assertEquals(9, result.get());
	}
}
