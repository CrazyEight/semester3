package Entry;

public abstract class EntryVisitor {
	
	private int result;
	
	public EntryVisitor(int result) {
		this.setResult(result);
	}
	public abstract void handleValue(Value value) throws StopException;
	public abstract void handleStopEntry(StopEntry entry);
	
	public int getResult() {
		return result;
	}
	
	public void setResult(int result) {
		this.result = result;
	}

}
