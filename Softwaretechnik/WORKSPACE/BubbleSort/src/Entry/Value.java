package Entry;

public class Value<E> extends Entry<E>
{
	private E wrappedElement;

	public Value(E element)
	{
		this.wrappedElement = element;
	}

	@Override
	public E getEntry()
	{
		return this.wrappedElement;
	}
	
	@Override
	public String toString()
	{
		return wrappedElement.toString();
	}
}
