package Entry;

public class StopEntry<E> extends Entry<E>{

	@Override
	public E getEntry() throws StopException {
		throw new StopException();
	}
	
	@Override
	public String toString()
	{
		return "Stop";
	}
}
