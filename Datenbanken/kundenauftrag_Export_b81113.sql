-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2018 at 10:24 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kundenauftrag`
--

-- --------------------------------------------------------

--
-- Table structure for table `abteilung`
--

CREATE TABLE `abteilung` (
  `ABID` char(10) NOT NULL,
  `Bezeichnung` char(50) DEFAULT NULL,
  `SID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abteilung`
--

INSERT INTO `abteilung` (`ABID`, `Bezeichnung`, `SID`) VALUES
('1.1', 'Verkauf', 'S1'),
('1.2', 'Verwaltung', 'S2'),
('1.3', 'Wartung', 'S1'),
('1.4', 'Lager', 'S3'),
('1.5', 'Kundendienst', 'S1');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `ARID` char(10) NOT NULL,
  `Artikelbezeichnung` char(100) DEFAULT NULL,
  `Preis` char(10) DEFAULT NULL,
  `Farbe` char(10) DEFAULT NULL,
  `Größe` char(10) DEFAULT NULL,
  `Gewicht` char(10) DEFAULT NULL,
  `AGID` char(10) NOT NULL,
  `LID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`ARID`, `Artikelbezeichnung`, `Preis`, `Farbe`, `Größe`, `Gewicht`, `AGID`, `LID`) VALUES
('1a', 'ProBook', '400,00', 'schwarz', '13,3 Zoll', '2040', 'A', 'L1'),
('2a', 'Enterprise_500', '200,00', 'weiß', '470x440x16', '42200', 'D', 'L1'),
('3a', 'ScanJet', '150,00', 'schwarz', '60x280x460', '2200', 'B', 'L2'),
('4a', 'Touch', '100,00', 'schwarz', '50x111x36', '88', 'C', 'L2'),
('5a', 'OfficeJet', '150,00', 'grau', '350x448x20', 'NULL', 'D', 'L1'),
('6a', 'SpaceMouse', '200,00', 'NULL', 'NULL', 'NULL', 'C', 'L2');

-- --------------------------------------------------------

--
-- Table structure for table `artikelgruppe`
--

CREATE TABLE `artikelgruppe` (
  `AGID` char(10) NOT NULL,
  `Artikelbezeichnung` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikelgruppe`
--

INSERT INTO `artikelgruppe` (`AGID`, `Artikelbezeichnung`) VALUES
('A', 'Laptop'),
('B', 'Scanner'),
('C', 'Maus'),
('D', 'Drucker'),
('E', 'Tastatur');

-- --------------------------------------------------------

--
-- Table structure for table `auftrag`
--

CREATE TABLE `auftrag` (
  `AID` char(10) NOT NULL,
  `AuftragsDatum` char(10) DEFAULT NULL,
  `Lieferdatum` char(10) DEFAULT NULL,
  `Status` char(10) DEFAULT NULL,
  `MID` char(10) NOT NULL,
  `KID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auftrag`
--

INSERT INTO `auftrag` (`AID`, `AuftragsDatum`, `Lieferdatum`, `Status`, `MID`, `KID`) VALUES
('1A', '11.07.2013', '22.08.2013', 'offen', 'M1', 'k1'),
('2A', '05.10.2012', '30.11.2012', 'geliefert', 'M2', 'k2'),
('3A', '22.05.2013', '11.06.2013', 'NULL', 'M1', 'k2'),
('4A', '09.10.2012', '31.10.2012', 'geliefert', 'M1', 'k3');

-- --------------------------------------------------------

--
-- Table structure for table `auftragsposition`
--

CREATE TABLE `auftragsposition` (
  `APID` char(10) NOT NULL,
  `Positionsmenge` char(10) DEFAULT NULL,
  `Positionswert` char(10) DEFAULT NULL,
  `Einzelpreisrabatt` char(10) DEFAULT NULL,
  `ARID` char(10) NOT NULL,
  `AID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auftragsposition`
--

INSERT INTO `auftragsposition` (`APID`, `Positionsmenge`, `Positionswert`, `Einzelpreisrabatt`, `ARID`, `AID`) VALUES
('1', '3', '540', '10', '2a', '1A'),
('2', '5', '500', NULL, '4a', '1A'),
('3', '1', '380', '5', '1a', '2A'),
('4', '3', '450', NULL, '3a', '2A'),
('5', '1', '380', '5', '1a', '3A'),
('6', '2', '360', '10', '2a', '3A'),
('7', '1', '150', NULL, '3a', '3A'),
('8', '4', '400', NULL, '4a', '4A');

-- --------------------------------------------------------

--
-- Table structure for table `kunde`
--

CREATE TABLE `kunde` (
  `KID` char(10) NOT NULL,
  `Land` char(50) DEFAULT NULL,
  `Name` char(25) DEFAULT NULL,
  `Wohnort` char(50) DEFAULT NULL,
  `Straße` char(50) DEFAULT NULL,
  `Vorname` char(25) DEFAULT NULL,
  `PLZ` char(5) DEFAULT NULL,
  `Anrede` char(25) DEFAULT NULL,
  `KGID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kunde`
--

INSERT INTO `kunde` (`KID`, `Land`, `Name`, `Wohnort`, `Straße`, `Vorname`, `PLZ`, `Anrede`, `KGID`) VALUES
('k1', 'Deuschland', 'Herrmann', 'Erlangen', 'Lange Zeile 88', 'Susanne', '91052', 'Frau', 'GR2'),
('k2', 'Deutschland', 'Schneider', 'München', 'Triebstraße 11a', 'Ger', '80993', 'Herr', 'GR1'),
('k3', 'Österreich', 'Kofler', 'Wien', 'Schönbrunner Straße 94', 'Gerhard', '1050', 'Herr', 'GR3');

-- --------------------------------------------------------

--
-- Table structure for table `kundengruppe`
--

CREATE TABLE `kundengruppe` (
  `KGID` char(10) NOT NULL,
  `KundengruppenBez` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kundengruppe`
--

INSERT INTO `kundengruppe` (`KGID`, `KundengruppenBez`) VALUES
('GR1', 'A'),
('GR2', 'B'),
('GR3', 'C'),
('GR4', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `lieferant`
--

CREATE TABLE `lieferant` (
  `LID` char(10) NOT NULL,
  `Name` char(25) DEFAULT NULL,
  `Straße` char(50) DEFAULT NULL,
  `Ort` char(50) DEFAULT NULL,
  `PLZ` char(5) DEFAULT NULL,
  `Land` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lieferant`
--

INSERT INTO `lieferant` (`LID`, `Name`, `Straße`, `Ort`, `PLZ`, `Land`) VALUES
('L1', 'HP', 'Herrenberger Straße 140', 'Böblingen', '71043', 'Deutschland'),
('L2', 'Microsoft', 'Box27', '19493', 'Kista', 'Schweden');

-- --------------------------------------------------------

--
-- Table structure for table `mitarbeiter`
--

CREATE TABLE `mitarbeiter` (
  `MID_1` char(10) NOT NULL,
  `Name` char(25) DEFAULT NULL,
  `Vorname` char(25) DEFAULT NULL,
  `Einstelldatum` char(10) DEFAULT NULL,
  `Anrede` char(50) DEFAULT NULL,
  `Land` char(50) DEFAULT NULL,
  `MID_2` char(10) DEFAULT NULL,
  `ABID` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mitarbeiter`
--

INSERT INTO `mitarbeiter` (`MID_1`, `Name`, `Vorname`, `Einstelldatum`, `Anrede`, `Land`, `MID_2`, `ABID`) VALUES
('M1', 'Bauer', 'Norbert', '12.03.2008', 'Herr', 'Deutschland', NULL, '1.1'),
('M2', 'Graf', 'Carola', '22.07.2009', 'Frau', 'Deutschland', 'M1', '1.2'),
('M3', 'Schulze', 'Hubert', '12.01.2010', 'Herr', 'Deutschland', 'M4', '1.3'),
('M4', 'Stein', 'Simone', '07.04.2007', 'Frau', 'Deutschland', NULL, '1.3');

-- --------------------------------------------------------

--
-- Table structure for table `standort`
--

CREATE TABLE `standort` (
  `SID` char(10) NOT NULL,
  `Stadt` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standort`
--

INSERT INTO `standort` (`SID`, `Stadt`) VALUES
('S1', 'Nürnberg'),
('S2', 'Köln'),
('S3', 'Berlin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abteilung`
--
ALTER TABLE `abteilung`
  ADD PRIMARY KEY (`ABID`),
  ADD KEY `SID` (`SID`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`ARID`),
  ADD KEY `AGID` (`AGID`),
  ADD KEY `LID` (`LID`);

--
-- Indexes for table `artikelgruppe`
--
ALTER TABLE `artikelgruppe`
  ADD PRIMARY KEY (`AGID`);

--
-- Indexes for table `auftrag`
--
ALTER TABLE `auftrag`
  ADD PRIMARY KEY (`AID`),
  ADD KEY `MID` (`MID`),
  ADD KEY `KID` (`KID`);

--
-- Indexes for table `auftragsposition`
--
ALTER TABLE `auftragsposition`
  ADD PRIMARY KEY (`APID`),
  ADD KEY `ARID` (`ARID`),
  ADD KEY `AID` (`AID`);

--
-- Indexes for table `kunde`
--
ALTER TABLE `kunde`
  ADD PRIMARY KEY (`KID`),
  ADD KEY `KGID` (`KGID`);

--
-- Indexes for table `kundengruppe`
--
ALTER TABLE `kundengruppe`
  ADD PRIMARY KEY (`KGID`);

--
-- Indexes for table `lieferant`
--
ALTER TABLE `lieferant`
  ADD PRIMARY KEY (`LID`);

--
-- Indexes for table `mitarbeiter`
--
ALTER TABLE `mitarbeiter`
  ADD PRIMARY KEY (`MID_1`),
  ADD KEY `MID_2` (`MID_2`),
  ADD KEY `ABID` (`ABID`);

--
-- Indexes for table `standort`
--
ALTER TABLE `standort`
  ADD PRIMARY KEY (`SID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `abteilung`
--
ALTER TABLE `abteilung`
  ADD CONSTRAINT `abteilung_ibfk_1` FOREIGN KEY (`SID`) REFERENCES `standort` (`SID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`AGID`) REFERENCES `artikelgruppe` (`AGID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `artikel_ibfk_2` FOREIGN KEY (`LID`) REFERENCES `lieferant` (`LID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `auftrag`
--
ALTER TABLE `auftrag`
  ADD CONSTRAINT `auftrag_ibfk_1` FOREIGN KEY (`MID`) REFERENCES `mitarbeiter` (`MID_1`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `auftrag_ibfk_2` FOREIGN KEY (`KID`) REFERENCES `kunde` (`KID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `auftragsposition`
--
ALTER TABLE `auftragsposition`
  ADD CONSTRAINT `auftragsposition_ibfk_1` FOREIGN KEY (`ARID`) REFERENCES `artikel` (`ARID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `auftragsposition_ibfk_2` FOREIGN KEY (`AID`) REFERENCES `auftrag` (`AID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `kunde`
--
ALTER TABLE `kunde`
  ADD CONSTRAINT `kunde_ibfk_1` FOREIGN KEY (`KGID`) REFERENCES `kundengruppe` (`KGID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `mitarbeiter`
--
ALTER TABLE `mitarbeiter`
  ADD CONSTRAINT `mitarbeiter_ibfk_1` FOREIGN KEY (`MID_2`) REFERENCES `mitarbeiter` (`MID_1`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `mitarbeiter_ibfk_2` FOREIGN KEY (`ABID`) REFERENCES `abteilung` (`ABID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
